# gitea_proof

[Verifying my OpenPGP key: openpgp4fpr:A34E981D571C83F4B59C07120648490248D99990]

This is an OpenPGP proof that connects [my OpenPGP key](https://keyoxide.org/A34E981D571C83F4B59C07120648490248D99990) to [this Gitea](https://codeberg.org/Shea690901) account. For details check out https://keyoxide.org/guides/openpgp-proofs